*** Settings ***
Documentation    test hello world

*** Variables ***
${david}    David2

*** Test Cases ***
TC0101 test hellow world
    [Tags]    regression    ready
    Log    Hello world
    Log To Console    Hello world2

TC0102 test hellow world2
    [Tags]    regression    notready
    Log    Hellow world2

TC0103 test say hello To
    Say Hello To    Ton

TC0104 test say hello to friends
    Say Hello To Friends    Ton    Jame    Kim
    Say Hello To Friends    Jane    Marry
    Say Hello To Friends    name3=Ton    name2=Kitti    name1=Ploy
    Say Hello To Friends    Ton    Jame   ${david}

TC0105 test say hello by get my friends
    ${name1}    ${name2}    ${name3}    Get My Friends
    Say Hello To Friends    ${name1}    ${name2}    ${name3}

*** Keywords ***
Say Hello To
    [Arguments]    ${name}
    Log    ${name}
    Log To Console    ${name}

Say Hello To Friends
    [Arguments]    ${name1}    ${name2}    ${name3}=Kitti
    Log    hello ${name1} and ${name2}, I'm ${name3}
    Log To Console    hello ${name1} and ${name2}, I'm ${name3}

Get My Friends
    ${mary}    Set Variable    Mary2
    [Return]    ${mary}   ${david}    Kim