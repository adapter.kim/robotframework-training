*** Settings ***
Test Setup    Log    Test Setup
Test Teardown    Log    Test Teardown
Suite Setup    Log    Suite Setup
Suite Teardown    Log    Suite Teardown
Test Timeout    10 seconds

*** Test Cases ***
TC0601 test case 1
    [Setup]    Log    test case 1 Setup
    Log    test case 1 start
    Log    test case 1 completed
    [Teardown]    Log    test case 1 Teardown

TC0602 test case 2
    [Setup]    Log    test case 2 Setup
    Log    test case 2 start
    Fail    test case 2 fail
    Log    test case 2 completed
    [Teardown]    Log    test case 2 Teardown

TC0603 test case 3
    Log    test case 3 start
    Log    test case 3 completed

TC0604 test case 4
    Log    test case 4 start
    Sleep    20
    Log    test case 4 completed