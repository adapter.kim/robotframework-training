*** Settings ***
Documentation     Suite description
Library           RequestsLibrary
Library           XML
Library           OperatingSystem
Library           Collections

*** Test Cases ***
TC0801 test request wiki
    ${proxies}=    Create Dictionary
    create session    wiki   https://en.wikipedia.org
    ${response}=    GET On Session    wiki    /wiki/Robot_Framework
    log     ${response.text}

TC0802 test request XML
    # 1) request XML
    create session    tb    http://www.thomas-bayer.com
    ${response}=    GET On Session    tb    /sqlrest/PRODUCT/
    # 2) parse XML ->
    ${root}=    Parse XML    ${response.text}
    log    ${root}
    ${product list}=    get elements    ${root}    PRODUCT
    # loop each product
    ${sum}=    set variable    ${0}
    FOR    ${i}    IN    @{product_list}
        ${attributes}=    get element attributes    ${i}
        Log    ${attributes}
        ${type}=    Evaluate    type(${attributes})
        ${link}=    set variable    ${attributes}[{http://www.w3.org/1999/xlink}href]
        #    3) request product detail
        create session    pdetail    ${link}
        ${response}=    GET On Session    pdetail    ${EMPTY}
        #    4) parse XML product detail to get price
        ${price}=    get element text    ${response.text}    PRICE
        #    5) sum up
        ${sum}=    evaluate    ${sum}+${price}
    END
    log    sum all price = ${sum}

TC0803 test dummy api
    # 0) prepare data
    ${employee}=    create dictionary    name=netta    age=30    salary=20000
    ${headers}=    create dictionary    User-Agent=${EMPTY}
    create session    dummy    http://dummy.restapiexample.com    headers=${headers}
    # 1) check if name exists -> delete it
    check if name ${employee}[name] exists
    # 2) create employee then get id
    add employee ${employee}
    # 3) check if the employee is in system
    validate employee ${employee}
    # 4) update salary
    set to dictionary    ${employee}    salary=30000
    update employee ${employee}
    # 5) check if salary changed
    validate employee ${employee}
    # 6) delete the employee
    delete employee by id ${employee}[id]
    # 7) check if employee deleted
    employee id ${employee}[id] should be deleted

TC0803_1 delete dummy api
    ${headers}=    create dictionary    User-Agent=${EMPTY}
    create session    dummy    http://dummy.restapiexample.com    headers=${headers}
    ${resp}=    get request    dummy    /api/v1/employees
    ${employees}=    set variable     ${resp.json()}
    FOR    ${i}    IN RANGE    0    1000
        delete employee by id ${employees}[${i}][id]
    END
    #FOR    ${employee}    IN    @{employees}
    #    IF    ${employee}[id]>40000 and ${employee}[id]<50000    delete employee by id ${employee}[id]
    #END

TC0803_2 delete dummy api
    ${headers}=    create dictionary    User-Agent=${EMPTY}
    create session    dummy    http://dummy.restapiexample.com    headers=${headers}
    FOR   ${id}    IN RANGE    51001    52000
        delete employee by id ${id}
    END

*** Keywords ***
sum friends balance
    [Arguments]    ${friends}
    ${sum}=    set variable    ${0}
    FOR    ${f}    IN    @{friends}
        ${sum}=    evaluate    ${sum}+${f}[balance]
    END
    return from keyword    ${sum}

check if name ${name} exists
    ${resp}=    get request    dummy    /api/v1/employees
    ${employees}=    set variable     ${resp.json()}
    FOR    ${employee}    IN    @{employees}
        run keyword if    $employee['employee_name']==$name
        ...    run keywords    delete employee by id ${employee}[id]
        ...    AND    exit for loop
    END

add employee ${employee}
    ${headers}=    create dictionary    Content-Type=application/json
    ${body}=    evaluate    json.dumps($employee)    modules=json
    ${resp}=    post request    dummy    /api/v1/create    headers=${headers}    data=${body}
    should be equal as integers    ${resp.status_code}    200
    # collect id
    ${json}=    set variable    ${resp.json()}
    ${id}=    set variable    ${json}[id]
    set to dictionary    ${employee}    id=${id}

validate employee ${employee}
    ${resp}=    get request    dummy    /api/v1/employee/${employee}[id]
    ${json}=    set variable    ${resp.json()}
    should be equal as integers    ${resp.status_code}    200
    should be equal as integers    ${json}[id]    ${employee}[id]
    should be equal as strings      ${json}[employee_name]    ${employee}[name]
    should be equal as integers    ${json}[employee_age]    ${employee}[age]
    should be equal as integers    ${json}[employee_salary]    ${employee}[salary]

update employee ${employee}
    ${headers}=    create dictionary    Content-Type=application/json
    ${body}=    evaluate    json.dumps($employee)    modules=json
    ${resp}=    put request    dummy    /api/v1/update/${employee}[id]    headers=${headers}    data=${body}
    ${json}=    set variable    ${resp.json()}
    should be equal as integers    ${resp.status_code}    200

delete employee by id ${id}
    ${resp}=    delete request    dummy    /api/v1/delete/${id}
    should be equal as integers    ${resp.status_code}    200

employee id ${id} should be deleted
    ${resp}=    get request    dummy    /api/v1/employee/${id}
    should be equal as strings    ${resp.text}    false