*** Settings ***
Documentation       Suite description
Library     ExcellentLibrary
Library     Collections
Library     OperatingSystem
Library     String
Library     DateTime
Library     AppiumLibrary
Test Teardown    Close All Workbooks
*** Variables ***
${dataPath}    ${CURDIR}\\..\\resources\\data.xlsx

*** Test Cases ***
TC0701 test read excel
    Open Workbook    ${dataPath}    alias=wb1
    Switch Sheet    Sheet1
    ${dataSheet1}    Read Sheet Data    get_column_names_from_header_row=${TRUE}
    Log To Console    ${dataSheet1}
    Log To Console    ${dataSheet1}[1][CUSTOMER_NO]
    FOR    ${row}    IN    @{dataSheet1}
        Log To Console    ${row}
    END
    Close Workbook    wb1

TC0702 test write excel
    ${timeStamp}    Get Current Date    result_format=epoch
    ${timeStamp}    Get Current Date    result_format=%Y%m%d_%H%M%S
    ${newdataPath}    Replace String    ${dataPath}    .xlsx    _${timeStamp}.xlsx
    Copy File    ${dataPath}    ${newdataPath}
    Open Workbook    ${newdataPath}    alias=wb1
    Switch Sheet    Sheet1
    ${columnCont}    Get Column Count
    ${header}    Create Dictionary
    FOR    ${i}    IN RANGE    1    ${columnCont+1}
        ${columnName}    Read From Cell    (${i}, 1)
        Set To Dictionary    ${header}    ${columnName}=${i}
    END
    ${customerNo}    Set Variable    N0005
    ${name}    Set Variable    Timmy
    ${salary}    Set Variable    50000
    ${fundRate}    Set Variable    10
    Write To Cell    (${header}[CUSTOMER_NO], 6)    ${customerNo}
    Write To Cell    (${header}[NAME], 6)    ${name}
    Write To Cell    (${header}[FUND_RATE], 6)    ${fundRate}
    Write To Cell    (${header}[SALARY], 6)    ${salary}
    Save
    Close Workbook    wb1