*** Settings ***
Library    SeleniumLibrary
Library    String
Library    Collections
Test Teardown    Close All Browsers

*** Test Cases ***
TC0801 test web google
    Open Browser    https://www.google.com/   chrome
    Sleep    5

TC0802 test online calculator
    Open Browser    http://www.math.com/students/calculators/source/basic.htm   headlesschrome
    Set Selenium Speed    0.05
    Click Element    //input[contains(@value,'1')]
    Click Element    //input[contains(@value,'+')]
    Click Element    //input[contains(@value,'3')]
    Click Element    //input[@name='DoIt']
    ${result}    Get Value    //input[@name="Input"]
    Should Be Equal As Strings    ${result}    4

TC0803 test soucedemo invalid password
    Open Browser    https://www.saucedemo.com/   chrome
    Input Text    user-name    standard_user
    Input Text    password    invalid
    Click Element    login-button
    ${result}    Get Text    //h3[@data-test="error"]
    Should Be Equal As Strings    ${result}    Epic sadface: Username and password do not match any user in this service

TC0804 test soucedemo shopping cart
    #login
    Open Browser    https://www.saucedemo.com/   chrome
    Input Text    user-name    standard_user
    Input Text    password    secret_sauce
    Click Element    login-button
    Wait Until Element Is Visible   //span[@class="title" and text()="Products"]
    #select item
    ${items}    Create List    Sauce Labs Backpack    Sauce Labs Bolt T-Shirt
    ${price}    Create List
    FOR    ${item}    IN    @{items}
        ${itemPrice}    Get Text    //div[text()="${item}"]/../../..//div[@class="inventory_item_price"]
        ${itemPrice}    Fetch From Right    ${itemPrice}    $
        Append To List   ${price}    ${itemPrice}
        Click Element    //div[text()="${item}"]/../../..//button[text()="Add to cart"]
    END
    #check cart item count
    ${count}    Get Length    ${items}
    ${cartBadge}    Get Text    //span[@class="shopping_cart_badge"]
    Should Be Equal As Strings    ${count}    ${cartBadge}
    Click Element    //span[@class="shopping_cart_badge"]/..
    Wait Until Element Is Visible   //span[@class="title" and text()="Your Cart"]
    #check cart
    ${itemCount}    Get Element Count    //div[@class="cart_item"]
    Should Be Equal As Strings    ${count}    ${itemCount}
    #check select item
    FOR    ${item}    IN    @{items}
        Element Should Be Enabled    //div[@class="inventory_item_name" and text()="${item}"]
    END
    Click Button    Checkout
    Wait Until Element Is Visible   //span[@class="title" and text()="Checkout: Your Information"]
    #check out info
    Input Text    first-name    Jirath
    Input Text    last-name    Eakthitiworranun
    Input Text    postal-code   10210
    Click Element    continue
    Wait Until Element Is Visible   //span[@class="title" and text()="Checkout: Overview"]
    #verify amount tax, total
    ${sum}    Set Variable    ${0}
    FOR    ${itemPrice}    IN    @{price}
        ${sum}    Evaluate    ${sum}+${itemPrice}
    END
    ${tax}    Evaluate    round(${sum}*0.08,2)
    ${total}    Evaluate    round(${sum}*1.08,2)
    ${getSum}    Get Text    //div[@class="summary_subtotal_label"]
    ${getSum}    Fetch From Right    ${getSum}    $
    ${getTax}    Get Text    //div[@class="summary_tax_label"]
    ${getTax}    Fetch From Right    ${getTax}    $
    ${getTotal}    Get Text    //div[@class="summary_total_label"]
    ${getTotal}    Fetch From Right    ${getTotal}    $
    Should Be Equal As Strings    ${sum}    ${getSum}
    Should Be Equal As Strings    ${tax}    ${getTax}
    Should Be Equal As Strings    ${total}    ${total}
    Click Button    finish
    Wait Until Element Is Visible   //span[@class="title" and text()="Checkout: Complete!"]