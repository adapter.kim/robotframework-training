*** Settings ***
Documentation    test calculator
Resource    ../keyword/calculator keywords.resource

*** Test Cases ***
TC0201 test add operation
    [Tags]    calculator    add
    # 1) prepare test data
    ${x}    Set Variable    10
    ${y}    Set Variable    5
    # ${expected result}    Set Variable    15
    ${expected result}    Set Variable    ${15}
    # 2) do test
    ${actual result}    Do Add operation    ${x}    ${y}
    # 3) validate result
    # Should Be Equal As Integers    ${expected result}    ${actual result}
    Should Be Equal    ${expected result}    ${actual result}

TC0202 test subtract operation
    [Tags]    calculator   subtract
    # 1) prepare test data
    ${x}    Set Variable    10
    ${y}    Set Variable    5
    ${expected result}    Set Variable    ${5}
    # 2) do test
    ${actual result}    Do Subtract operation    ${x}    ${y}
    # 3) validate result
    Should Be Equal    ${expected result}    ${actual result}

TC0203 test multiply operation
    [Tags]    calculator   multiply
    # 1) prepare test data
    ${x}    Set Variable    10
    ${y}    Set Variable    5
    ${expected result}    Set Variable    ${50}
    # 2) do test
    Do Multiply operation    ${x}    ${y}    ${expected result}
    # 3) validate result
    # Should Be Equal    ${expected result}    ${actual result}

TC0204 test multiply operation data driven
    [Template]    Do Multiply operation
    #x    #y    #exected result
    1    2    ${2}
    5    5    ${25}
    10   10    ${100}

TC0205 test divide operation gherkins style
    given user input 10 and 2
    when user click divide button
    then monitor should show 5