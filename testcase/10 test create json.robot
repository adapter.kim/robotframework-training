*** Settings ***
Documentation     Suite description
Library           OperatingSystem
Library           RequestsLibrary
Library           Collections

*** Test Cases ***
TC1001 test create json from data file
    ${content}=    get file    ${CURDIR}/../resources/test data.json
    log    ${content}

TC1002 test create json from template file
    ${name}=    set variable    Mark
    ${age}=    set variable    40
    ${sex}=    set variable    male
    ${content}=    get file    ${CURDIR}/../resources/test data template.json
    ${json_text}=    replace variables    ${content}
    log    ${json_text}

TC1003 test create json at runtime
    ${name}=    set variable    John
    ${age}=    set variable    40
    ${sex}=    set variable    male
    # friends
    ${jenny}=    create dictionary    name=Jenny    age=20
    ${mike}=    create dictionary    name=Mike    age=25
    ${friends}=    create list    ${jenny}    ${mike}
    ${json}=    create dictionary    name=${name}    age=${age}    sex=${sex}    friends=${friends}
    ${type_json}=    Evaluate    type(${json})
    log    ${type_json}
    ${json_text}=    evaluate    json.dumps($json)    modules=json
    ${type_json_text}=    Evaluate    type(${json_text})
    log    ${type_json_text}
    log    ${json_text}

TC1004 test create json hybrid
    ${name}=    set variable    Paul
    ${age}=    set variable    50
    ${sex}=    set variable    male
    ${content}=    get file    ${CURDIR}/../resources/test data template dynamic.json
    # friends
    ${jenny}=    create dictionary    name=Jenny    age=20
    ${mike}=    create dictionary    name=Mike    age=25
    ${friends}=    create list    ${jenny}    ${mike}
    ${friends}=    evaluate    json.dumps($friends)    modules=json
    ${json_text}=    replace variables    ${content}
    log    ${json_text}
