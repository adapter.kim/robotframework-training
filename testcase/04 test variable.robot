*** Settings ***
Library    Collections
Variables    ../resources/data.yaml

*** Test Cases ***
TC0401 test scalor variable
    ${x}    Set Variable    ${10}
    ${y}    Set Variable    ${10.5}
    ${z}    Set Variable    ZZZ
    ${type_x}    Evaluate    type($x)
    ${type_y}    Evaluate    type($y)
    ${type_z}    Evaluate    type($z)
    Should Be True    type($x)==int
    Should Be True    type($y)==float
    Should Be True    type($z)==str

TC0402 test list Variable
    ${list1}    Create List    1    5    3    10
    Log    ${list1}
    Log List    ${list1}
    Log    ${list1}[3]
    Log    ${list1[3]}

    # slice
    log    ${list1}[0:3]
    log    ${list1}[1:3]
    log    ${list1}[1:]
    log    ${list1}[0:-1]
    log    ${list1}[0:-2]

    ${test101}    Set Variable    test101
    log    ${test101}[0:-2]

    # set list value
    Set List Value    ${list1}    1    200
    log    ${list1}

    # append
    Append To List    ${list1}    5    6    7    7
    log    ${list1}    

    # remove
    Remove From List    ${list1}    2
    log   ${list1}    

    # loop by length
    ${len}    Get Length    ${list1}
    FOR    ${i}    IN RANGE    0    ${len}
        Log    ${i}
        Log    item=${list1}[${i}]
    END

    # loop by for in
    FOR    ${item}    IN    @{list1}
        Log    ${item}
    END

TC0403 test dictionary valiable
    # 1) item in dict must have name(key)
    # 2) name must uniqe
    # 3) no order
    ${dict1}    Create Dictionary    a=1    b=2   c=3    f=${6}    g=hello    7=700    ${8}=800    8=888   a=abc
    Log    ${dict1}
    Log Dictionary    ${dict1}
    log    ${dict1}[c]
    log    ${dict1["c"]}
    log    ${dict1.c}
    log    ${dict1}[${8}]
    log    ${dict1}[8]
    log    ${dict1[8]}
    log    ${dict1['8']}

    set to dictionary    ${dict1}    x=900    y=1000    b=200
    log dictionary    ${dict1}

    Remove from dictionary    ${dict1}    7    ${8}
    log dictionary    ${dict1}

    #loop
    #for in dictinary
    FOR    ${i}    IN    @{dict1}
        Log    ${i}
        Log    ${dict1}[${i}]
    END

    #for in zip
    FOR    ${key}    ${value}    IN ZIP    ${dict1.keys()}    ${dict1.values()}
        Log    ${key}
        Log    ${value}
    END

TC0404 test nested structure
    # 2 dimensions list
    ${sublist1}=    create list    11    12    13    14
    ${sublist2}=    create list    21    22    23    24
    ${sublist3}=    create list    31    32    33    34
    ${sublist4}=    create list    41    42    43    44
    ${mainlist}=    create list    ${sublist1}    ${sublist2}    ${sublist3}    ${sublist4}
    log list    ${mainlist}
    log    ${mainlist}[2][3]

    # dict of dict
    ${subdict1}=    create dictionary    a=11    b=12    c=13    d=14
    ${subdict2}=    create dictionary    a=21    b=22    c=23    d=24
    ${subdict3}=    create dictionary    a=31    b=32    c=33    d=34
    ${subdict4}=    create dictionary    a=41    b=42    c=43    d=44
    ${maindict}=    create dictionary    dict1=${subdict1}    dict2=${subdict2}    dict3=${subdict3}    dict4=${subdict4}
    log dictionary    ${maindict}
    log    ${maindict}[dict3][c]

    # hybrid
    ${subdict1}=    create dictionary    a=11    b=12    c=13    d=14
    ${subdict2}=    create dictionary    a=21    b=22    c=23    d=24
    ${subdict3}=    create dictionary    a=31    b=32    c=33    d=34
    ${subdict4}=    create dictionary    a=41    b=42    c=43    d=44

    ${sublist1}=    create list    100    200
    ${sublist2}=    create list    ${subdict3}    ${subdict4}

    ${maindict}=    create dictionary    item1=ITEM1    item2=${subdict1}    item3=${subdict2}    item4=${sublist1}    item5=${sublist2}
    log dictionary    ${maindict}
    ${pp}=    evaluate    pprint.pformat(${maindict})    modules=pprint
    log    ${pp}

TC0405 test valiable form yaml
    Log    ${string}
    Log    ${integer}
    Log    ${list}[0]
    Log    ${dict}[twodimention][two]
    Log    ${dict.twodimention.two}

TC0406 test valiable from command line
    Log    ${username}