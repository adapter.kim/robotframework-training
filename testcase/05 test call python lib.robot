*** Settings ***
Library    ../lib/helper.py

*** Test Cases ***
TC0501 test say hello from python
    ${text}   Say Hello To    Kim
    Log    ${text}

TC0502 test sum list
   ${sumall}    Sum List    10    10
   Log    ${sumall}

TC0503 test call python lib
   ${x}   Create List    ${1}    ${2}    ${3}    ${4}    ${5}
   ${y}   Create List    6    7    8    9    10
   ${sum}    Calculate Two Arrays    ${x}    ${y}

TC0504 test return multiple values
    ${name1}    ${name2}    ${name3}    Get My Friends
    Log    ${name1},${name2} and ${name3}

TC0505 test call inline python method
    FOR    ${counter}    IN RANGE    1    10
        ${result}    Evaluate    random.randint(1,10)    modules=random
        Log    ${result}
        Log To Console    ${result}
    END

TC0506 test fail from python
    Fail From Python