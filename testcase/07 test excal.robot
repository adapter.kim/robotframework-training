*** Settings ***
Library    ExcellentLibrary
Library    OperatingSystem
Library    Collections
Library    DateTime
Library    String

*** Variables ***
${ExcelPath}    ${CURDIR}\\..\\resources\\data.xlsx

*** Test Cases ***
TC0701 test read excel
    Open Workbook    ${ExcelPath}    alias=wb1
    Switch Sheet    Sheet1
    ${data}    Read Sheet Data
    Log    ${data}
    Log List    ${data}
    FOR    ${row}    IN    @{data}
        IF    '${row}[0]'=='Y'
            Log To Console    ${row}[2]
        END
    END
    [Teardown]    Close Workbook    alias=wb1

TC0702 test read excel with header
    Open Workbook    ${ExcelPath}    alias=wb1
    Switch Sheet    Sheet1
    ${data}    Read Sheet Data    get_column_names_from_header_row=True
    Log    ${data}
    Log List    ${data}
    FOR    ${row}    IN    @{data}
        IF    '${row}[RUN_TAG]' == 'Y'
            Log To Console    ${row}[NAME]
        END
    END
    [Teardown]    Close Workbook    alias=wb1

TC0703 test write excel
    ${currentDate}    Get Current Date    result_format=epoch
    ${currentDate}    Get Current Date    result_format=%Y%m%d_%H%M%S
    Log To Console    ${currentDate}
    ${NewExcelPath}    Replace String    ${ExcelPath}    .xlsx    _${currentDate}.xlsx
    Log To Console    ${NewExcelPath}
    Copy File    ${ExcelPath}    ${NewExcelPath}
    Open Workbook    ${NewExcelPath}    alias=wb2
    Switch Sheet    Sheet1
    ${columnCount}    Get Column Count
    ${header}    Create Dictionary
    FOR    ${i}    IN RANGE    1    ${columnCount+1}
        ${columnName}    Read From Cell   (${i}, 1)
        Log To Console    ${columnName}
        Set To Dictionary    ${header}    ${columnName}=${i}
    END
    ${nameTag}    Set Variable    Y
    ${customerNo}    Set Variable    N0005
    ${name}    Set Variable    Kim
    ${salary}    Set Variable    1000
    ${fundRate}    Set Variable    7
    Write To Cell    (${header}[RUN_TAG], 6)    ${nameTag}
    Write To Cell    (${header}[CUSTOMER_NO], 6)    ${customerNo}
    Write To Cell    (${header}[NAME], 6)    ${name}
    Write To Cell    (${header}[SALARY], 6)    ${salary}
    Write To Cell    (${header}[FUND_RATE], 6)    ${fundRate}
    Save
    [Teardown]    Close Workbook    alias=wb2
