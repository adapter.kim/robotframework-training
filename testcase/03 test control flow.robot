*** Test Cases ***
TC0301 Test if condition
    ${x}    Set Variable    ${20}
    IF    ${x} > 5
        Log To Console    ${x} greater than 5
    ELSE IF    ${x} < 5
        Log To Console    ${x} lesser than 5
    ELSE
        Log To Console    ${x} equal 5
    END
    IF    ${x} == ${20}
        Log To Console    1111
        Log To Console    2222
        Log To Console    3333
    END
    ${hello}    Set Variable    hel'lo
    IF    $hello=='hello'
        Log To Console    String is Equal
    ELSE
        Log To Console    String is not Equal
    END

TC0302 Test if condition 2
    IF    ${True}
        Log    This line IS executed Line1.
        Log    This line IS executed Line2.
    END
    IF    ${False}
        Log    This line is NOT executed.
    END
    IF    "cat" == "cat"
        Log    This line IS executed.
    END
    IF    "cat" != "dog"
        Log    This line IS executed.
    END
    IF    "cat" == "dog"
        Log    This line is NOT executed.
    END
    IF    "cat" == "cat" and "dog" == "dog"
        Log    This line IS executed.
    END
    IF    "cat" == "cat" and "dog" == "cat"
        Log    This line is NOT executed.
    END
    IF    1 == 1
        Log    This line IS executed.
    END
    IF    2 < 1
        Log    This line is NOT executed.
    END
    IF    2 <= 2
        Log    This line IS executed.
    END
    IF    len("cat") == 3
        Log    This line IS executed.
    END
    IF    (1 == 1 and 2 == 2) and 3 == 3
        Log    This line IS executed since the expressions evaluate to True.
    END
    IF    (1 == 2 or 3 == 4) or 3 == 3
        Log    This line IS executed since one of the expressions evaluates to True.
    END 

TC0303 Test for loop
    FOR    ${i}    IN RANGE    2    11    
        Log    ${i}
    END
    FOR    ${i}    IN RANGE    0    11    2
        Log    ${i}
    END

TC0304 Test nested for loop
    #sum =0
    #for i = 1 to 10
    #  for j = 1 to 10
    #    sum = sum+1
    ${sum}   Set Variable    0
    FOR    ${i}    IN RANGE    1    11
        FOR    ${j}    IN RANGE    1    11
            ${sum}    Evaluate    ${sum}+${1}
        END        
    END
    Log    sum=${sum}
    Log To Console    sum=${sum}

TC0305 testt continue and exit for loop
    FOR    ${i}    IN RANGE    0    10
        Continue For Loop If    ${i} % 2 == 1
        Exit For Loop If    ${i}>6
        Log To Console    ${i}
    END

TC0306 Test do while
    FOR    ${counter}    IN RANGE    1    10000
        Log    ${counter}
        Exit For Loop If    ${counter}>=10
    END
