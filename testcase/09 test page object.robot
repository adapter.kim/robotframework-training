*** Settings ***
Resource    ../pom/sauce_demo/login.resource
Resource    ../pom/sauce_demo/product list.resource
Resource    ../pom/sauce_demo/cart.resource

Suite Teardown      close site

*** Test Cases ***
TC0901 test sauce demo
    ${items}=    create list    Sauce Labs Bike Light    Sauce Labs Bolt T-Shirt    Sauce Labs Onesie
    open and login with valid account
    select ${items} into cart
    checkout ${items}
    input buyer information
    confirm order
