from pprint import pformat

def say_hello_to(name):
    text = "Hello " + name
    return text

def sum_list(max_i, max_j):
    sum = 0
    for i in range(1,int(max_i)):
        for j in range(1,int(max_j)):
            sum = sum + (i*j)
    return sum


def calculate_two_arrays(list1, list2):
    sum = 0
    for i in list1:
        for j in list2:
            sum += int(i)*int(j)
    return sum

def  get_my_friends():
    return "John","Mary","David"


def pretty_json(j):
    return pformat(j)

def sum_list_from_list(*mylist):
    sum = 0
    for i in mylist:
        sum += i
    return sum

def sum_list_from_scalar(mylist):
    sum = 0
    for i in mylist:
        sum += i
    return sum


def fail_from_python():
    raise Exception("fail")


#x = [1,2,3,4,5]
#y = [6,7,8,9,10]

##print calculate_two_arrays(x, y)
